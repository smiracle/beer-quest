WARNING: DO NOT DISTRIBUTE THIS APPLICATION TO OTHERS WITHOUT MY PERMISSION

===DISCLAIMER===
This early version of the application, "Beer Quest", is for evaluation purposes only and is not for commercial sale or distribution.

===HOW TO INSTALL AND RUN===
1) Download the compressed file.
2) Double click the compressed file and go through the menus to unpack the game files automatically.
3) Double click the Game.exe file (it has a red dragon icon) and allow it to run. Ignore any warnings that it's a virus. It's not.
Note: The game can only run on Windows. And no, there is no way to port it to another OS.

===OPTIMIZING GAME DISPLAY===
The game is generally best played in windowed mode which keeps the frame rate relatively low. However, it looks best when viewed in full-screen, press Alt+Enter to toggle full-screen. The game looks better on non-widescreen monitors because graphics are not stretched horizontally quite so much.

===CONTROLS===
The game is controlled entirely by keyboard (or gamepad if you have it plugged in). Controls can be reconfigured by hitting F1 in game:
'W' Key - Cancel a selection. Press to leave a menu.
'A' Key - Open the main menu. Cancels a selection if already in a menu.
'S' Key - Confirm a selection. Press to interact with characters and objects in the game world.
'D' Key - Hold to walk instead of run. Will also cancel selections.
Arrow Keys - control movement and menu traversal.
'A' Key - Press when targeting an enemy in battle to display useful information about that enemy type. Use 'Q' and 'R' to view more menus related to that enemy.
'E' Key - Scroll to next menu. Press to toggle the scope of certain spells and abilities in battle. Can be used to access more enemy information in battle.
'Q' Key - Scroll to previous menu. Press to toggle the scope of certain spells and abilities in battle. Can be used to access more enemy information in battle.
'TAB' Key - Toggle targeting of allies and enemies in battle. Only works with certain skills and abilities.
Alt+Enter - Toggle full-screen. (See optimizing game display)

===KNOWN ISSUES as of v2.0===
-Caterpillar followers will frequently overlap the leader.
-Player is frequently overlapped when moving in front of sprites and certain dynamically overlapping objects.
-Framerate sometimes dips on larger maps. Closing other applications may prevent this.
-Clipping occurs when the screen pans beyond a large sprite. Not many large sprites were used, so this is uncommon.
-Battler poses are sometimes not immediately reset, especially after reviving an ally.
-Marcel's delayed summons sometimes cause allies to perform the casting animation.
-Equipment is often not optimized correctly, for example using dual daggers with Eric is much better than shortbow.
-Shortbows sometimes do not display an attack animation, no idea why.
-Crashes can rarely occur when traversing menus rapidly

===CREDITS===
*SCRIPTS*
Victor Sant - Victor Engine, Animated Battle, Active Time Battle, Actor Battlers, State Graphics, Actor Events, Counter Options, Action Counter, Light Effects, Target, Toggle Target, Leap Attack, Damage Popup, State Auto Apply, Fog and Overlay, Fixed Parallax, Anti-lag, Direct Command, Custom Vehicle, Custom Damage Slip Effect
Yanfly - Party System, Skill System, Target Info, Steal command, Input Combination Skills, ForceMove, Save Engine, Region Battlebacks, Message System, JP Manager, Victory Aftermath, Target Manager, System Options, Shop Options, Status Menu, Scan, Scan Add-on, Element Absorb
Tsukihime - Show Multiple Choice, Hide Choices, State Animations, Text Input, Region fog, Bit Switches, Test Play, Custom Use Conditions, Memorize Cursor, Change Class, Item rarity colors, Input Interpreter Add-on
GaryCXJk - Input Remapping and controller support
Modern Algebra - Text formatting, Animated Parallaxes
Galv - Map Positions, Vehicle Restrictions
Formar - Counter Attack Skills
NeonBlack - Stairs and Custom Movement, Zombie
Mobychan - Correct Sprite Display
TDS - Battleback stretch
Racheal - Relocated pause graphic
Trihan - Two handed weapon fix
Zane - Full-screen Resolution
??? - Skip Title Scene
***Note: Many scripts were edited from their original form***
*MAP GRAPHICS*
Squaresoft - World map tiles, world of balance town buildings, world of ruin town buildings, nikeah graphics, sailing ship graphic, west-east bridge, machinery, flowers, barrels, clocks, piles of items, skinny trees, beds, torch compartments, castle structures, cave walls, house interiors, almost all floors and terrain
Celiana - Trees, crops, carpets, paintings, shelves, gallows, vines, bridge in Union, treehouses, digging tools, hay bales, bunkbeds, wooden floors, plants, maps, tables
Mack - Mountain tiles
??? - Basis for underlying marsh graphic
??? - Basis for bridge background, snowy mountain background
??? - City graphics on world map
??? - Basis for fountain in front of Livermore castle
Enterbrain (RPG Maker) - Some graphics on world map, such as Union trees and NW Castle
*SPRITE GRAPHICS*
Squaresoft: Doors, Chests, Locke, Arvis, Male/Female NPCs, basis for summoner sprite, basis for ranger sprite, Eric's sprite,
basis for Carlo's sprite, 
??? - DK Cecil, Paladin Cecil, Kain, Red Mage, White Mage, Black Mage, Edward Sprite, Rydia Sprite, Tellah Sprite, Ninja, Tara's sprite, basis for Marcel's sprite
FEOK & Poco Loco - Pirate sprites 
Mack - Animal sprites
Enterbrain (RPG Maker) - Mine cart, vehicles, switches, bird, sheep
??? - Squall and Lightning sprites for statues
*BATTLER GRAPHICS*
Squaresoft: Almost all battler graphics are from FF6 and FF1: Dawn of Souls, Elven Wizard is from FF5
??? - Shroomly
*BATTLEBACK GRAPHICS*
Squaresoft: Almost all battlebacks are from FF1: Dawn of souls, some are from FF6
*ANIMATION GRAPHICS*
Enterbrain (RPG Maker) - Almost all animations
Timmah - Various particle effects
Squaresoft - Moogle, moomba
Toasty Cheese Dazzling Effects (not for commercial use)
??? - Spider graphic
*CHARACTER PORTRAITS*
J. E. H. - All portraits were done by J. E. H. except Trevor's
Enterbrain (RPG Maker) - Evil portrait
*ICON GRAPHICS*
???
*MISCELLANEOUS GRAPHICS*
Squaresoft - Basis for castle & bridge in intro
??? - Title screen art
*MUSIC*
Squaresoft - Modified versions of songs from FF7, FF6, FF4, and FF1
Nintendo - Super Mario RPG forest theme
Flying Wild Hog - Shadow Warriors theme
??? - Various 8 bit music tracks
ICOM Simulations - Shadowgate dungeon track
*SOUND EFFECTS*
Squaresoft - Various sounds from FF Tactics, FF7, FF6, and Chrono Trigger
Enterbrain (RPG Maker) - Atmospheric sound effects such as drip and wind, various skill sounds